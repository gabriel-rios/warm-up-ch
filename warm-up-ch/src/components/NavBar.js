import React, {useContext} from 'react';
import {DataContext} from '../context/DataContext';
import {Link} from 'react-router-dom';

const NavBar = () => {
    const {setPost} = useContext(DataContext);
    return(
        <div className="d-flex justify-content-around py-2">
            <Link to="/"><button className="btn btn-primary mr-2" onClick={() => setPost()}>Home</button></Link>
            <Link to="/post"><button className="btn btn-info">Add Post</button></Link>
        </div>
    )
}

export default NavBar;
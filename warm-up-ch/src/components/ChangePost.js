import React, {useContext, useEffect, useState} from 'react';
import {DataContext} from '../context/DataContext';
import './Styles.css';

const ChangePost = (props) => {

    const {data, post, addPost, editPost} = useContext(DataContext);
    const [title, setTitle] = useState();
    const [body, setBody] = useState();

    const onTitleChange = (e) => {
        setTitle(e.target.value);
    }
    const onBodyChange = (e) => {
        setBody(e.target.value);
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        const data = {
            title: title,
            body: body
        };
        // If you're editing post you need to pass both id and content
        if(post){
            editPost(post.id, data)
        }
        else{
            console.log('agregando ')
            addPost(data);
        }
    }

    useEffect( () => {
        if(post){
            document.getElementById("title").value=post.title;
            document.getElementById("body").value=post.body;
        }
    }, [post]);

    return(
        <div className="container viewContainer">
        {post ?
            <form onSubmit={handleSubmit}>
                {/* if there is a post assigned, initials values will be applied to the form */}
                <fieldset>
                    <label>
                        <p>Title: </p><input required onChange={onTitleChange} id="title" name="title" />
                    </label>
                </fieldset>
                <fieldset>
                    <label>
                        <p>Body: </p><input required onChange={onBodyChange} id="body" name="body" />
                    </label>
                </fieldset>
                <button className="btn btn-success" type="submit">Submit</button>
            </form>
            : 
                <form onSubmit={handleSubmit}>
                    <fieldset>
                        <label>
                            <p>Title: </p><input required onChange={onTitleChange} name="title" placeholder="Title" />
                        </label>
                    </fieldset>
                    <fieldset>
                        <label>
                            <p>Body: </p><input required onChange={onBodyChange} name="body" placeholder="Body" />
                        </label>
                    </fieldset>
                    <button className="btn btn-success" type="submit">Submit</button>
                </form>
            }
        </div>
    )
}

export default ChangePost;
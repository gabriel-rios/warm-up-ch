import React, {useContext} from 'react';
import {Link} from 'react-router-dom';
import {DataContext} from '../context/DataContext';

const Home = (props) => {
    const {data, setPost, deletePost} = useContext(DataContext);

    return(
        <div className="container mt-3">
            {data.length > 0 ? 
                data.map(i => (
                    <div key={i.id} className="my-1 d-flex justify-content-between border-bottom border-light">
                        {i.title}
                            <div className="pb-2">
                                <Link to="/completePost">
                                    <button className="btn btn-success ml-2" onClick={() => setPost(i)}>See</button>
                                </Link>
                                    <button  className="btn btn-danger mx-2" onClick={() => deletePost(i.id)}>Delete</button>
                                <Link to="/post">
                                    <button  className="btn btn-warning" onClick={() => setPost(i)}>Edit</button>
                                </Link>
                            </div>
                    </div>
                ))
            : <div>Spinner placeholder</div>}
        </div>
    )
}

export default Home;
import React, {useContext, useEffect} from 'react';
import {DataContext} from '../context/DataContext';
import './Styles.css';

const CompletePost = (props) => {
    const {data, post} = useContext(DataContext);

    return(
        <div className="container viewContainer mt-4">
            <h1 className="text-center">{post.title}</h1>
            <p>{post.body}</p>
        </div>
    )
}

export default CompletePost;
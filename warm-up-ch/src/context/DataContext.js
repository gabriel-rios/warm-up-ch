import React, {useEffect, useState} from 'react';
import axios from 'axios';
import cogoToast from 'cogo-toast';

const DataContext = React.createContext();

const DataProvider = (props) => {
    const url = "https://jsonplaceholder.typicode.com/posts/";
    const [result, setResult] = useState([]);
    // const [loadToggle, setLoadToggle] = useState(true);
    const [post, setPost] = useState();

    useEffect( () =>{
        axios.get(url)
        .then(res => {
            setResult(res.data)
        })
    // }, [loadToggle]);
    }, []);

    const addPost = (post) => {
        axios.post(`${url}`, post)
        .then(res =>
            cogoToast.success('Post added'))
        .catch(res =>
            cogoToast.error('Could not add the post'))
    }

    const deletePost = (id) => {
        axios.delete(`${url}${id}`)
        .then(res =>
            cogoToast.success('Post deleted'))
        .catch(res =>
            cogoToast.error('Could not delete the post'))
    }

    const editPost = (id, post) => {
        axios.put(`${url}${id}`, post)
        .then(res =>
            cogoToast.success('User edited'))
        .catch(res =>
            cogoToast.error('Could not edit the post'))
    }

    return(
        <>
            <DataContext.Provider
                value={{
                    data: result,
                    // loadToggle: loadToggle,
                    post: post,
                    setPost: setPost,
                    addPost: addPost,
                    deletePost: deletePost,
                    editPost: editPost
                }}
            >
                {props.children}
            </DataContext.Provider>
        </>
    )
}

export {DataProvider, DataContext}
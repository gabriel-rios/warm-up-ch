import React from 'react';
import {DataProvider} from './context/DataContext';
import{
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';
import Home from './components/Home';
import ChangePost from './components/ChangePost';
import CompletePost from './components/CompletePost';
import NavBar from './components/NavBar';
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="bg-dark text-light">
      <Router>
        <DataProvider>
          <NavBar />
          <Switch>
            <Route path="/post">
              <ChangePost />
            </Route>
            <Route path="/completePost">
              <CompletePost />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </DataProvider>
      </Router>
    </div>
  );
}

export default App;
